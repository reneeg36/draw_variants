#!/usr/bin/env python

import numpy as np

from track import Track

import rpy2.robjects as robjects



class TranscriptTrack(Track):
    '''Class for drawing a single transcript'''

    def __init__(self, transcript, options):
        super(TranscriptTrack, self).__init__(options)
        self.transcript = transcript
        

    def draw_coding_region(self, r, coord, plot_coords):
        # add 1 b/c drawn coords are beween start and end
        # x_coord = r.c(coord.start, coord.end+1,
        #               coord.end+1, coord.start)
        
        plot_start = np.where(plot_coords == coord.start)[0][0]
        plot_end = np.where(plot_coords == coord.end)[0][0]

        x_coord = r.c(plot_start, plot_end,
                      plot_end, plot_start)

        y_coord = r.c(self.top, self.top,
                      self.bottom, self.bottom)

        r.polygon(x_coord, y_coord, border=self.border_color,
                  col=self.color)


    def draw_non_coding_region(self, r, coord, plot_coords):
        # make non-coding portions 2/3 of height
        h = self.top - self.bottom
        mid = (self.top + self.bottom) * 0.5
        nc_top = mid + h*0.33
        nc_bottom = mid - h*0.33

        # add 1 b/c drawn coords are between start and end
        # x_coord = r.c(coord.start, coord.end+1,
        #               coord.end+1, coord.start)

        plot_start = np.where(plot_coords == coord.start)[0][0]
        plot_end = np.where(plot_coords == coord.end)[0][0]

        x_coord = r.c(plot_start, plot_end,
                      plot_end, plot_start)
        y_coord = r.c(nc_top, nc_top,
                      nc_bottom, nc_bottom)

        r.polygon(x_coord, y_coord, border=self.border_color,
                  col=self.color)


    def draw_exon(self, r, exon, plot_coords):
        cds_start = self.transcript.cds_start
        cds_end = self.transcript.cds_end

        if (self.transcript.is_coding() and 
            cds_start <= exon.end and
            cds_end >= exon.start):
            # some portion of this exon is coding
            
            coord = exon.copy()

            if cds_start > exon.start:
                coding_start = cds_start
                coding_end = exon.end

                # draw non-coding portion before cds start
                coord.start = exon.start
                coord.end = cds_start - 1
                self.draw_non_coding_region(r, coord, plot_coords)
            else:
                coding_start = exon.start

            if cds_end < exon.end:
                coding_end = cds_end
                
                # draw non-coding portion after cds end
                coord.start = cds_end + 1
                coord.end = exon.end
                self.draw_non_coding_region(r, coord, plot_coords)
            else:
                coding_end = exon.end


            # draw coding portion
            coord.start = coding_start
            coord.end = coding_end
            self.draw_coding_region(r, coord, plot_coords)

        else:
            # this is a non-coding exon
            self.draw_non_coding_region(r, exon, plot_coords)



    def draw_intron(self, r, intron, plot_coords, break_size=0.25):
        '''Draws introns as lines with a break'''

        plot_start = np.where(plot_coords == intron.start)[0][0]
        plot_end = np.where(plot_coords == intron.end)[0][0]
        intron_len = plot_end - plot_start + 1
        plot_mid = plot_start + (intron_len/2)

        # draw two intron lines to make a break in the middle
        break_len = intron_len*(break_size/2)
        break_start = plot_mid - break_len
        break_end = plot_mid + break_len

        y_mid = (self.top + self.bottom) * 0.5
        y = r.c(y_mid, y_mid)

        x_left = r.c(plot_start, break_start)
        x_right = r.c(break_end, plot_end)

        r.lines(x_left, y, col=self.border_color)
        r.lines(x_right, y, col=self.border_color)

        # draw the break lines
        gene_h = self.top - self.bottom
        break_h = gene_h * 0.05
    
        y1 = y_mid + break_h
        y2 = y_mid - break_h
        y = r.c(y1, y2)

        x1_left = break_start + break_len
        x2_left = break_start - break_len

        x1_right = break_end + break_len
        x2_right = break_end - break_len

        if intron.strand == -1:
            x_left = r.c(x2_left, x1_left)
            x_right = r.c(x2_right, x1_right)
        else:
            x_left = r.c(x1_left, x2_left)
            x_right = r.c(x1_right, x2_right)

        r.lines(x_left, y, col=self.border_color)
        r.lines(x_right, y, col=self.border_color)


    def draw_track(self, r, plot_coords):
        tr = self.transcript
        
        # draw exons
        for ex in tr.exons:
            self.draw_exon(r, ex, plot_coords)

        # draw introns 
        for intron in tr.get_introns():
            self.draw_intron(r, intron, plot_coords)
