#!/usr/bin/env python

import coord


class Transcript(coord.Coord):
    def __init__(self, name=None, exons=[], cds_start=None,
                 cds_end=None):
        '''Creates and initializes a new transcript object'''

        self.chrom = None
        self.start = None
        self.end = None
        self.strand = None
        self.exons = []
        
        for ex in exons:
            self.add_exon(ex)

        for ex in exons:
            ex.position = self.assign_exon_position(ex)

        self.name = name
        self.cds_start = cds_start
        self.cds_end = cds_end

    
    def n_exons(self):
        '''Returns the number of exons in this transcript'''
        return len(self.exons)


    def size(self):
        '''Returns the combined length of exons in this transcript'''
        ttl_size = 0
        for ex in self.exons:
            ttl_size += ex.length()
            
        return ttl_size

    
    def is_coding(self):
        '''Returns true if this transcrpt is protein-coding'''
        return self.cds_start is not None


    def add_exon(self, exon):
        '''Adds an exon to this transcript. If the exon falls outside 
        of the coordinates of this transcript, the transcript coordinates 
        are updated'''
        
        if self.n_exons() == 0:
            # this if the first exon added to the transcript
            self.chrom = exon.chrom
            self.start = exon.start
            self.end = exon.end
            self.strand = exon.strand
        else:
            if exon.chrom != self.chrom:
                raise ValueError("Exon chromosome %s does not match "
                                 "transcript chromosome %s" % 
                                 (exon.chrom, self.chrom))

            if exon.strand != self.strand:
                raise ValueError("Exon strand %d does not match "
                                 "transcript strand %d" % 
                                 (exon.strand, self.strand))

            if exon.start < self.start:
                self.start = exon.start
            if exon.end > self.end:
                self.end = exon.end

        self.exons.append(exon)


    def assign_exon_position(self, ex):
        '''Returns the position of the exon in the transcript, 
        options are first, last, middle, or single'''
       
        if len(self.exons) == 1:
            return 'single'
        elif ex == self.exons[0]:
            return 'first'
        elif ex == self.exons[-1]:
            return 'last'
        else:
            return 'middle'


    def get_introns(self):
        '''Returns a list of coordinates that represent the introns
        for this transcript'''
        introns = []

        for i in range(len(self.exons)-1):
            ex1 = self.exons[i]
            ex2 = self.exons[i+1]

            if self.strand == -1:
                intron = coord.Coord(self.chrom, ex2.start-1, ex1.end+1,
                                     strand=self.strand)
            else:
                intron = coord.Coord(self.chrom, ex1.end+1, ex2.start-1,
                                     strand=self.strand)

            introns.append(intron)
        
        return introns


    def __str__(self):
        name_str = "NA"
        if self.name is not None:
            name_str = self.name

        strand_str = "0"
        if self.strand is not None:
            strand_str = str(self.strand)

        cds_start_str = "NA"
        if self.cds_start is not None:
            cds_start_str = str(self.cds_start)

        cds_end_str = "NA"
        if self.cds_end is not None:
            cds_end_str = str(self.cds_end)

        exon_starts = [str(ex.start) for ex in self.exons]
        exon_ends = [str(ex.end) for ex in self.exons]

        exon_start_str = ",".join(exon_starts)
        exon_end_str = ",".join(exon_ends)

        fields = [name_str, self.chrom, str(self.start), 
                  str(self.end), strand_str,
                  exon_start_str, exon_end_str, cds_start_str,
                  cds_end_str]

        return "\t".join(fields)



def read_transcripts(path):
    '''Retrieves all transcripts from the specified transcript file'''

    f = open(path, "r")
    header = f.readline()
    
    transcripts = []

    for line in f:
        words = line.rstrip().split("\t")
        tr_name = words[1]
        chrom = words[2]
        start = int(words[3])
        end = int(words[4])
        strand = int(words[5])
        exon_starts = [int(x) for x in words[6].split(",")]
        exon_ends = [int(x) for x in words[7].split(",")]

        # parse CDS start/end
        if words[8] == "NA":
            cds_start = None
        else:
            cds_start = int(words[8])

        if words[9] == "NA":
            cds_end = None
        else:
            cds_end = int(words[9])

        exons = []
        for i in range(len(exon_starts)):
            exon = coord.Coord(chrom, exon_starts[i], exon_ends[i], strand)
            exons.append(exon)

        tr = Transcript(name=tr_name, exons=exons, cds_start=cds_start,
                        cds_end=cds_end)
        
        transcripts.append(tr)

    f.close()
    return transcripts
