#!/usr/bin/env python

class CoordError(Exception):
    """An exception indicating that something is wrong with a coordinate"""
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)


class Coord(object):
    def __init__(self, chrom, start, end, strand=0, name=None):
        self.chrom = chrom
        self.start = start
        self.end = end
        self.strand = strand
        self.name = name

        if start > end:
            raise CoordError("start (%d) should be less than or "\
                             "equal to end (%d)" % (start, end))
        if start < 1:
            raise CoordError("start (%d) should not be less than 1" % (start))

        if strand != 0 and strand != 1 and strand != -1:
            raise CoordError("strand should be one of (-1, 0, 1)")

        
    def copy(self):
        '''Creates a copy of this coordinate object'''
        return Coord(self.chrom, self.start, self.end,
                     strand=self.strand)


    def length(self):
        '''Returns the size of the region spanned by the
        coordinate in bases'''
        return self.end - self.start + 1

        
    def __str__(self):
        '''returns a string representation of this coordinate'''
        if self.name:
            name_str = self.name + " "
        else:
            name_str = ""

        coord_str = "%s%s:%d-%d (%d)" % (name_str, self.chrom, 
                                         self.start, self.end, 
                                         self.strand)
        return coord_str
            
