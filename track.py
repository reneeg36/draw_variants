#!/usr/env/bin python


class Track(object):
    '''An abstract base class to draw different types of genome tracks'''

    def __init__(self, options):
        self.left = None
        self.right = None
        self.top = None
        self.bottom = None

        if 'height' in options:
            self.height = float(options['height'])
        else:
            self.height = 1.0

        if 'cex' in options:
            self.cex = float(options['cex'])
        else:
            self.cex = 1.0

        if 'track_label' in options:
            self.track_label = options['track_label']
        else:
            self.track_label = ""

        if 'color' in options:
            self.color = options['color']
        else:
            self.color = "grey90"

        if 'border_color' in options:
            self.border_color = options['border_color']
        else:
            self.border_color = "black"


    def draw_track_label(self, r, color="black"):
        '''Draws a label on the left side of the track'''

        y_mid = self.top - (self.top - self.bottom) / 2

        # start text coordinate 1% of the way into the track
        x = self.left + (self.right - self.left + 1) * 0.01

        r.text(x=x, y=y_mid, pos=4, labels=self.track_label,
               cex=self.cex, col=color)


    def set_position(self, left, right, top, bottom):
        self.left = left
        self.right = right
        self.top = top
        self.bottom = bottom

    
    def draw_track(self, r):
        if self.track_label:
            self.draw_track_label(r)
