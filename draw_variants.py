#!/usr/bin/env python

import sys

import rpy2.robjects as robjects
from rpy2.robjects.packages import importr

import argparse

import numpy as np

import coord
import transcript
import track

from transcripttrack import TranscriptTrack



def get_args():
    parser = argparse.ArgumentParser(description="Generates plots "
                                     "transcripts and their variants")
    parser.add_argument("--transcript", required=True, 
                       help="Ensemble transcript ID to plot")
    parser.add_argument("tr_file", help="path to file containing "
                        "transcript models")
    return parser.parse_args()


def map_coordinates(tr, exon_flank=10):
    '''Creates a plotting array of genomic coordinates for the transcript. 
    Introns are reduced to 10 plotting positions'''

    # calculate length of plotting array, assuming 10 positions for each intron
    array_len = tr.size() + (2*exon_flank)*(tr.n_exons()-1)
    plot_coords = np.zeros(array_len, dtype=np.int32)

    i = 0
    for ex in tr.exons:

        if ex.position == "single":
            start = ex.start
            end = ex.end
        elif ex.position == "first":
            start = ex.start
            end = ex.end + exon_flank
        elif ex.position == "last":
            start = ex.start - exon_flank
            end = ex.end
        elif ex.position == "middle":
            start = ex.start - exon_flank
            end = ex.end + exon_flank

        block_len = end - start + 1
        plot_coords[i:i+block_len] = np.arange(start, end+1)
        i += block_len

    return plot_coords


def draw_tracks(r, tracks, plot_coords, tr, margin=0.10):
    
    cur_y = -margin

    # set the position of each of the tracks
    for t in tracks:
        top = cur_y
        bottom = top - t.height
        t.set_position(0, len(plot_coords), top, bottom)
        cur_y = bottom - margin

    # create an empty plot
    # if tr.strand == -1:
    #     # flip transcripts on the minus strand so all face 5' to 3'
    #     xlim = r.c(len(plot_coords), 0)
    # else:
    #     xlim = r.c(0, len(plot_coords))

    xlim = r.c(0, len(plot_coords))
    
    top = 0
    bottom = cur_y
    ylim = r.c(bottom, top)

    xlab = ""

    r.plot(r.c(0), r.c(0), type="n", xlim=xlim, ylim=ylim,
           yaxt="n", xaxt="n", xlab=xlab, ylab="", bty="n")

    for t in tracks:
        t.draw_track(r, plot_coords)


def main():
    args = get_args()
    tr_path = args.tr_file
    tr_name = args.transcript

    sys.stderr.write("reading transcripts and finding %s\n" % tr_name)
    trs = transcript.read_transcripts(tr_path)
    tr_dict = dict([(tr.name, tr) for tr in trs])
    tr = tr_dict[tr_name]

    exon_flank = int(0.025*tr.size())
    plot_coords = map_coordinates(tr, exon_flank=exon_flank)

    r = robjects.r

    grdevices = importr('grDevices')
    grdevices.pdf(file=tr.name+".pdf", width=8, height=5)

    tracks = []

    options = {"track_label" : "hello"}

    tr_track = TranscriptTrack(tr, options)
    tracks.append(tr_track)

    draw_tracks(r, tracks, plot_coords, tr)

    grdevices.dev_off()


main()
